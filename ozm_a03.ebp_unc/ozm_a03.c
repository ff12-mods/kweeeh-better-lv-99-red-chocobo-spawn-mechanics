// File size: 41584 Bytes
// Author:    t-hirose
// Source:    ozm_a03.src
// Date:      00/00 00:00
// Binary:    section_000.bin

option authorName = "t-hirose/ffgriever";
option fileName = "ozm_a03-chocobo.src";
option dataFile = "ozm_a03.src.data";
option spawnOrder = {2, -1, 1, -1, -1, -1, -1, -1};
option positionFlags = 0;
option unknownFlags1 = 0;
option unknownScale = {100, 100, 100};
option unknownPosition2d = {1032, 768};

//======================================================================
//                Global and scratchpad variable imports                
//======================================================================
import global   short   シナリオフラグ = 0x0;
import global   short   battle_global_flag[64] = 0xa72;
import global   float   mp_map_set_angle = 0x900;
import global   float   mp_map_set_x = 0x904;
import global   float   mp_map_set_y = 0x908;
import global   float   mp_map_set_z = 0x90c;
import global   char    mp_last_weather = 0x940;
import global   char    mp_4map_choco = 0x942;
import scratch2 u_char  scratch2_var_13 = 0x40;


//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_a;              // pos: 0xe;
u_char  file_var_b;              // pos: 0xf;
u_char  file_var_c;              // pos: 0x10;
u_char  file_var_d;              // pos: 0x11;
u_char	redChocoHighChance;
u_char  redChocoLowChance;       // pos: 0x14;


script setup(0)
{

	function init()
	{
		return;
	}


	function battle()
	{
		btlAtelSetupActorStart();
		btlAtelSetDecidingEntryPointTimes(3);
		btlAtelSetActiveActorMax(6);
		file_var_a = 30;
		file_var_b = 7;
		file_var_c = 8;
		file_var_d = 0;
		if (シナリオフラグ >= 0x7530)
		{
			redChocoLowChance = 100;
			redChocoHighChance = 100;
		}
		else
		{
			redChocoLowChance = 10;
			redChocoHighChance = 25;
		}
		btlAtelSetEntryStruct(0x30001b0);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_14;            // pos: 0x0;
	char    local_var_15;            // pos: 0x1;

	function map()
	{
		local_var_14 = 0;
		sysDVAR(55, local_var_14);
		if (シナリオフラグ < 0x79e)
		{
			local_var_15 = (rand_29(100) + 1);
			if (local_var_15 <= 100)
			{
				sysDVAR(56, local_var_15);
				setweatherslot(1);
				mp_last_weather = 1;
				local_var_14 = 1;
			}
			else if (local_var_15 <= 100)
			{
				sysDVAR(56, local_var_15);
				setweatherslot(2);
				mp_last_weather = 2;
				local_var_14 = 1;
			}
			else
			{
				sysDVAR(56, local_var_15);
				setweatherslot(3);
				mp_last_weather = 3;
				local_var_14 = 1;
			}
		}
		else
		{
			if ((nowmap() == 0x215 && (getweatherslot() == 3 || getweatherslot() == 2)))
			{
				steppoint(100);
				setweatherslot(2);
			}
			if (lastmap() == 247)
			{
				local_var_15 = (rand_29(100) + 1);
				if (local_var_15 <= 60)
				{
					sysDVAR(56, local_var_15);
					setweatherslot(1);
					mp_last_weather = 1;
					local_var_14 = 1;
				}
				else if (local_var_15 <= 100)
				{
					sysDVAR(56, local_var_15);
					setweatherslot(2);
					mp_last_weather = 2;
					local_var_14 = 1;
				}
				else
				{
					sysDVAR(56, local_var_15);
					setweatherslot(3);
					mp_last_weather = 3;
					local_var_14 = 1;
				}
			}
			if (lastmap() == 0x108)
			{
				switch (mp_last_weather)
				{
					case 1:
						local_var_15 = (rand_29(100) + 1);
						if (local_var_15 <= 60)
						{
							sysDVAR(56, local_var_15);
							setweatherslot(1);
							mp_last_weather = 1;
							local_var_14 = 1;
						}
						else if (local_var_15 <= 80)
						{
							sysDVAR(56, local_var_15);
							setweatherslot(2);
							mp_last_weather = 2;
							local_var_14 = 1;
						}
						else
						{
							sysDVAR(56, local_var_15);
							setweatherslot(3);
							mp_last_weather = 3;
							local_var_14 = 1;
						}
						break;
					case 2:
						local_var_15 = (rand_29(100) + 1);
						if (local_var_15 <= 0)
						{
							sysDVAR(56, local_var_15);
							setweatherslot(1);
							mp_last_weather = 1;
							local_var_14 = 1;
						}
						else if (local_var_15 <= 60)
						{
							sysDVAR(56, local_var_15);
							setweatherslot(2);
							mp_last_weather = 2;
							local_var_14 = 1;
						}
						else
						{
							sysDVAR(56, local_var_15);
							setweatherslot(3);
							mp_last_weather = 3;
							local_var_14 = 1;
						}
						break;
					case 3:
						local_var_15 = (rand_29(100) + 1);
						if (local_var_15 <= 0)
						{
							sysDVAR(56, local_var_15);
							setweatherslot(1);
							mp_last_weather = 1;
							local_var_14 = 1;
						}
						else if (local_var_15 <= 60)
						{
							sysDVAR(56, local_var_15);
							setweatherslot(2);
							mp_last_weather = 2;
							local_var_14 = 1;
						}
						else
						{
							sysDVAR(56, local_var_15);
							setweatherslot(3);
							mp_last_weather = 3;
							local_var_14 = 1;
						}
						break;
				}
			}
			if (lastmap() == 0x1bf)
			{
				steppoint(102);
				local_var_15 = (rand_29(100) + 1);
				if (local_var_15 <= 70)
				{
					sysDVAR(56, local_var_15);
					setweatherslot(1);
					mp_last_weather = 1;
					local_var_14 = 1;
				}
				else if (local_var_15 <= 100)
				{
					sysDVAR(56, local_var_15);
					setweatherslot(2);
					mp_last_weather = 2;
					local_var_14 = 1;
				}
				else
				{
					sysDVAR(56, local_var_15);
					setweatherslot(3);
					mp_last_weather = 3;
					local_var_14 = 1;
				}
			}
			if (((lastmap() == 0x246 || lastmap() == 0x2ef) || lastmap() == 0x2c8))
			{
				steppoint(101);
				local_var_15 = (rand_29(100) + 1);
				if (local_var_15 <= 60)
				{
					sysDVAR(56, local_var_15);
					setweatherslot(1);
					mp_last_weather = 1;
					local_var_14 = 1;
				}
				else if (local_var_15 <= 80)
				{
					sysDVAR(56, local_var_15);
					setweatherslot(2);
					mp_last_weather = 2;
					local_var_14 = 1;
				}
				else
				{
					sysDVAR(56, local_var_15);
					setweatherslot(3);
					mp_last_weather = 3;
					local_var_14 = 1;
				}
			}
		}
		return;
	}
}


script __MJ_CTRL000(0)
{

	function init()
	{
		reqenable(12);
		setmapjumpgroup(1);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_16;            // pos: 0x0;

	function mapjump(12)
	{
		local_var_16 = 1;
		if (local_var_16)
		{
			regI3 = sysucoff();
			if (regI3)
			{
				clearmapjumpstatus();
				sysucon();
			}
			else
			{
				mp_last_weather = getweatherslot();
				spotsoundtrans(40, 0);
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, 12);
				for (regI0 = 1; regI0 <= 15; regI0 = (regI0 + 1))
				{
					setmapidmj(regI0, 1, 1);
				}
				if ((check_move_battlestatus(0) == 1 || istownmap() == 1))
				{
					ucmove_52b(0, 204.942535, -0, 187.700638);
				}
				if ((check_move_battlestatus(1) == 1 || istownmap() == 1))
				{
					ucmove_52b(1, 204.942535, -0, 187.700638);
				}
				if ((check_move_battlestatus(2) == 1 || istownmap() == 1))
				{
					ucmove_52b(2, 204.942535, -0, 187.700638);
				}
				if ((check_move_battlestatus(3) == 1 || istownmap() == 1))
				{
					ucmove_52b(3, 204.942535, -0, 187.700638);
				}
				wait(12);
				stopspotsound();
				pausesestop();
				fadesync();
				wait(2);
				mapjump(0x20e, 4, 0);
			}
		}
		return;
	}
}


script __MJ_CTRL001(0)
{

	function init()
	{
		reqenable(12);
		setmapjumpgroup(2);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_17;            // pos: 0x0;
	char    local_var_18;            // pos: 0x1;
	char    local_var_19;            // pos: 0x2;

	function mapjump(12)
	{
		local_var_17 = 0;
		local_var_18 = 1;
		if (getchocobomodebit())
		{
			sethpmenu(0);
			ucoff();
			settrapshowstatus(0);
			movecancel();
			wait(8);
			local_var_18 = 0;
			setmeswincaptionid(1, 0);
			setmeswincaptionid(1, 0);
			askpos(1, 0, 127);
			local_var_19 = aaske(1, 0x1000000);
			mesclose(1);
			messync(1, 1);
			if (local_var_19 == 0)
			{
				steppoint(1);
				suspendbattle();
				steppoint(2);
				fadeout_d0(2, 30);
				fadesync_d3(2);
				setchocobomode(0);
				partyallread();
				releasemapjumpgroupflag(115);
				mapjump(0x2ef, 1, 1);
			}
			else
			{
				local_var_18 = 0;
				getmapjumpposbyindex(2, &mp_map_set_x, &mp_map_set_y, &mp_map_set_z, &mp_map_set_angle);
				sysReqew(1, map_chocomj::map_choco_move);
				clearmapjumpstatus();
				setnoupdatebehindcamera(0);
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
		}
		if (local_var_18)
		{
			regI3 = sysucoff();
			if (regI3)
			{
				clearmapjumpstatus();
				sysucon();
			}
			else
			{
				mp_last_weather = getweatherslot();
				spotsoundtrans(24, 0);
				goto localjmp_5;
				goto localjmp_6;
			localjmp_5:
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, 12);
			localjmp_6:
				getmapdestposbyindex(3, &mp_map_set_x, &mp_map_set_y, &mp_map_set_z, &mp_map_set_angle);
				for (regI0 = 1; regI0 < 15; regI0 = (regI0 + 1))
				{
					setmapidmj(regI0, 1, 1);
				}
				if ((check_move_battlestatus(0) == 1 || istownmap() == 1))
				{
					ucmove_52b(0, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				if ((check_move_battlestatus(1) == 1 || istownmap() == 1))
				{
					ucmove_52b(1, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				if ((check_move_battlestatus(2) == 1 || istownmap() == 1))
				{
					ucmove_52b(2, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				if ((check_move_battlestatus(3) == 1 || istownmap() == 1))
				{
					ucmove_52b(3, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				wait(14);
				stopspotsound();
				pausesestop();
				hideparty();
				voicestopall();
				setbattlethinkstatus_freetarget_group(-1, 0);
				mapjump(0x2ef, 1, 0);
			}
		}
		if (local_var_17)
		{
			regI3 = sysucoff();
			if (regI3)
			{
				clearmapjumpstatus();
				sysucon();
			}
			else
			{
				mp_last_weather = getweatherslot();
				spotsoundtrans(40, 0);
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, 12);
				for (regI0 = 1; regI0 <= 15; regI0 = (regI0 + 1))
				{
					setmapidmj(regI0, 1, 1);
				}
				if ((check_move_battlestatus(0) == 1 || istownmap() == 1))
				{
					ucmove_52b(0, -20.4244308, -0, 328.383392);
				}
				if ((check_move_battlestatus(1) == 1 || istownmap() == 1))
				{
					ucmove_52b(1, -20.4244308, -0, 328.383392);
				}
				if ((check_move_battlestatus(2) == 1 || istownmap() == 1))
				{
					ucmove_52b(2, -20.4244308, -0, 328.383392);
				}
				if ((check_move_battlestatus(3) == 1 || istownmap() == 1))
				{
					ucmove_52b(3, -20.4244308, -0, 328.383392);
				}
				wait(12);
				stopspotsound();
				pausesestop();
				fadesync();
				wait(2);
				mapjump(0x2ef, 1, 0);
			}
		}
		return;
	}
}


script __MJ_CTRL002(0)
{

	function init()
	{
		reqenable(12);
		setmapjumpgroup(3);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_1a;            // pos: 0x0;

	function mapjump(12)
	{
		local_var_1a = 1;
		if (local_var_1a)
		{
			regI3 = sysucoff();
			if (regI3)
			{
				clearmapjumpstatus();
				sysucon();
			}
			else
			{
				mp_last_weather = getweatherslot();
				spotsoundtrans(40, 0);
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, 12);
				for (regI0 = 1; regI0 <= 15; regI0 = (regI0 + 1))
				{
					setmapidmj(regI0, 1, 1);
				}
				if ((check_move_battlestatus(0) == 1 || istownmap() == 1))
				{
					ucmove_52b(0, 276.44751, -0, 306.767731);
				}
				if ((check_move_battlestatus(1) == 1 || istownmap() == 1))
				{
					ucmove_52b(1, 276.44751, -0, 306.767731);
				}
				if ((check_move_battlestatus(2) == 1 || istownmap() == 1))
				{
					ucmove_52b(2, 276.44751, -0, 306.767731);
				}
				if ((check_move_battlestatus(3) == 1 || istownmap() == 1))
				{
					ucmove_52b(3, 276.44751, -0, 306.767731);
				}
				wait(12);
				stopspotsound();
				pausesestop();
				fadesync();
				wait(2);
				mapjump(0x218, 3, 0);
			}
		}
		return;
	}
}


script btl_trap_ctrl(0)
{

	function init()
	{
		modelread(0x3000001);
		modelreadsync(0x3000001);
		settrapresource(0x3000001);
		return;
	}
}


script 常駐ディレクター(0)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		if (nowjumpindex() == 2)
		{
			setmapjumpgroupflag(66);
		}
		return;
	}
}


script Map_Director(0)
{

	function init()
	{
		if (!(getchocobomodebit()))
		{
			releasemapjumpgroupflag(115);
		}
		else
		{
			setmapjumpgroupflag(115);
		}
		return;
	}
}


script PC00(5) : 0x80
{

	function init()
	{
		setupbattle(0);
		return;
	}
}


script PC01(5) : 0x81
{

	function init()
	{
		setupbattle(1);
		return;
	}
}


script PC02(5) : 0x82
{

	function init()
	{
		setupbattle(2);
		return;
	}
}


script PC03(5) : 0x83
{

	function init()
	{
		setupbattle(3);
		return;
	}
}


script map_chocomj(6)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	float   local_var_1b;            // pos: 0x0;

	function init()
	{
		local_var_1b = 0.150000006;
		switch (nowmap())
		{
			case 0x15a:
			case 132:
				if (mp_4map_choco)
				{
					releasemapjumpgroupflag(0x12f);
				}
				else
				{
					setmapjumpgroupflag(0x12f);
				}
				break;
			case 157:
				if (mp_4map_choco)
				{
					releasemapjumpgroupflag(0x130);
				}
				else
				{
					setmapjumpgroupflag(0x130);
				}
				break;
			case 0x18f:
				if (mp_4map_choco)
				{
					releasemapjumpgroupflag(0x131);
				}
				else
				{
					setmapjumpgroupflag(0x131);
				}
				break;
			case 0x1c2:
			case 0x1cb:
				if (mp_4map_choco)
				{
					releasemapjumpgroupflag(0x132);
				}
				else
				{
					setmapjumpgroupflag(0x132);
				}
				break;
			case 217:
				if (mp_4map_choco)
				{
					releasemapjumpgroupflag(0x133);
				}
				else
				{
					setmapjumpgroupflag(0x133);
				}
				break;
		}
		return;
	}


	function map_choco_move()
	{
		capturepc(-1);
		if (getchocobomode())
		{
			local_var_1b = 0.270000011;
		}
		if (getsummonmode())
		{
			usecharhit(0);
		}
		move_5e(mp_map_set_x, mp_map_set_y, mp_map_set_z, local_var_1b);
		turn_62(mp_map_set_angle);
		if (getsummonmode())
		{
			usecharhit(1);
		}
		releasepc();
		return;
	}
}


script treasure_00(6)
{

	function init()
	{
		setuptreasure(0x3000300);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000300);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000300);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000300);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_01(6)
{

	function init()
	{
		setuptreasure(0x3000318);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000318);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000318);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000318);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_02(6)
{

	function init()
	{
		setuptreasure(0x3000330);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000330);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000330);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000330);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_03(6)
{

	function init()
	{
		setuptreasure(0x3000348);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000348);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000348);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000348);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_04(6)
{

	function init()
	{
		setuptreasure(0x3000360);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000360);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000360);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000360);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_05(6)
{

	function init()
	{
		setuptreasure(0x3000378);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000378);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000378);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000378);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_06(6)
{

	function init()
	{
		setuptreasure(0x3000390);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000390);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000390);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000390);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_07(6)
{

	function init()
	{
		setuptreasure(0x30003a8);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x30003a8);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x30003a8);
		sethpmenu_436(1, 5);
		talktreasureafter(0x30003a8);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_08(6)
{

	function init()
	{
		setuptreasure(0x30003c0);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x30003c0);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x30003c0);
		sethpmenu_436(1, 5);
		talktreasureafter(0x30003c0);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_09(6)
{

	function init()
	{
		setuptreasure(0x30003d8);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x30003d8);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x30003d8);
		sethpmenu_436(1, 5);
		talktreasureafter(0x30003d8);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_10(6)
{

	function init()
	{
		setuptreasure(0x30003f0);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x30003f0);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x30003f0);
		sethpmenu_436(1, 5);
		talktreasureafter(0x30003f0);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
char    file_var_8;              // pos: 0xc;


script npc_battle_00(7)
{

	spawnData(1, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x300016e);
		btlAtelSetPoint2(0x3000028);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(12);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(21, 0);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(2);
		btlAtelSetBelong(7, 0);
		btlAtelSetEntryFlag(0x4000);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000000);
		btlAtelSetAbility(0x48020000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (シナリオフラグ >= 0x654)
		{
			if (btlAtelDecideEntryGroupByContinuous() == -1)
			{
				file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
				if (file_var_8 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_8, -1);
					setentrygroup(file_var_8);
					requestreentry();
					return;
				}
			}
			if ((btlAtelGetDeadCountNow(0) < btlAtelGetDeadCountMax(0) || btlAtelGetDeadCountMax(0) == 0))
			{
				if ((btlAtelGetElapseTime(0) > btlAtelGetRespawnTime(0) || btlAtelGetDeadCountNow(0) == 0))
				{
					if (rand_29(100) < file_var_a)
					{
						btlAtelSetKeepWorkGroup(0, -1);
						setentrygroup(0);
						requestreentry();
						return;
					}
				}
			}
		}
		while (true)
		{
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}
}


script unique_monster_00(7)
{

	spawnData(1, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x3000170);
		btlAtelSetPoint2(0x300004a);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(11);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(45, 0);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(4, 0);
		btlAtelSetEntryFlag(0x8000);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000008);
		btlAtelSetAbility(-0x3f7f5fff, 0xc808020);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		while (true)
		{
			if ((getweatherslot() == 2 && シナリオフラグ >= 0x654))
			{
				if ((btlAtelGetDeadCountNow(0) < btlAtelGetDeadCountMax(0) || btlAtelGetDeadCountMax(0) == 0))
				{
					if ((btlAtelGetElapseTime(0) > btlAtelGetRespawnTime(0) || btlAtelGetDeadCountNow(0) == 0))
					{
						btlAtelSetKeepWorkGroup(0, -1);
						setentrygroup(0);
						requestreentry();
						btlAtelIgnoreKeepWork(0, 0);
						return;
					}
				}
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_e;              // pos: 0x12;
short   file_var_10;             // pos: 0x16;
float   file_var_11;             // pos: 0x18;
float   file_var_12;             // pos: 0x1c;


script event_garif_00(7)
{

	spawnData(1, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x3000172);
		btlAtelSetPoint2(0x3000072);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(13);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(21, 0);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(2);
		btlAtelSetBelong(7, 0);
		btlAtelSetEntryFlag(0x4000);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000000);
		btlAtelSetAbility(0x48020000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if ((シナリオフラグ < 0x654 && battle_global_flag[2] == 0))
		{
			if (btlAtelDecideEntryGroupByContinuous() == -1)
			{
				file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
				if (file_var_8 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_8, -1);
					setentrygroup(file_var_8);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			if ((btlAtelGetDeadCountNow(0) < btlAtelGetDeadCountMax(0) || btlAtelGetDeadCountMax(0) == 0))
			{
				if ((btlAtelGetElapseTime(0) > btlAtelGetRespawnTime(0) || btlAtelGetDeadCountNow(0) == 0))
				{
					if (((シナリオフラグ < 0x654 && battle_global_flag[2] == 0) && file_var_e >= 1))
					{
						file_var_10 = btlAtelGetEntryPoint(0, (file_var_e - 1));
						file_var_11 = btlAtelGetPositionDataX(file_var_10);
						file_var_12 = btlAtelGetPositionDataZ(file_var_10);
						btlAtelSetKeepWorkGroup(0, -1);
						btlAtelIgnoreKeepWork(0, 0);
						setentrygroup(0);
						requestreentry();
						btlAtelSetEntryPositionForce(file_var_11, file_var_12);
						return;
					}
				}
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			battle_global_flag[2] = 1;
		}
		while (true)
		{
			wait(1);
		}
	}
}


script event_monster_00(7)
{

	spawnData(4, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x3000174);
		btlAtelSetPoint2(0x300008e);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if ((シナリオフラグ < 0x654 && battle_global_flag[2] == 0))
		{
			if (btlAtelDecideEntryGroupByContinuous() == -1)
			{
				file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
				if (file_var_8 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_8, -1);
					setentrygroup(file_var_8);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			if ((シナリオフラグ < 0x654 && battle_global_flag[2] == 0))
			{
				for (regI0 = 1; regI0 < 64; regI0 = (regI0 + 1))
				{
					file_var_10 = btlAtelGetEntryPoint(regI0, 0);
					if (file_var_10 != -1)
					{
						file_var_11 = btlAtelGetPositionDataX(file_var_10);
						file_var_12 = btlAtelGetPositionDataZ(file_var_10);
						if (distance_290(-1, file_var_11, file_var_12) < 42)
						{
							if (btlAtelGetAngleDiff(-1, file_var_11, file_var_12) < 90)
							{
								file_var_8 = regI0;
								break;
							}
						}
					}
					else
					{
						file_var_8 = 0;
						break;
					}
				}
				if (file_var_8 >= 1)
				{
					if ((btlAtelGetDeadCountNow(file_var_8) < btlAtelGetDeadCountMax(file_var_8) || btlAtelGetDeadCountMax(file_var_8) == 0))
					{
						if ((btlAtelGetElapseTime(file_var_8) > btlAtelGetRespawnTime(file_var_8) || btlAtelGetDeadCountNow(file_var_8) == 0))
						{
							file_var_e = file_var_8;
							btlAtelSetKeepWorkGroup(file_var_8, -1);
							btlAtelIgnoreKeepWork(file_var_8, 0);
							setentrygroup(file_var_8);
							requestreentry();
							return;
						}
					}
				}
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}


	function entry01()
	{
		btlAtelSetUnit(7);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn01()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if ((シナリオフラグ < 0x654 && battle_global_flag[2] == 0))
		{
			if (btlAtelDecideEntryGroupByContinuous() == -1)
			{
				file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
				if (file_var_8 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_8, -1);
					setentrygroup(file_var_8);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			if ((シナリオフラグ < 0x654 && battle_global_flag[2] == 0))
			{
				for (regI0 = 1; regI0 < 64; regI0 = (regI0 + 1))
				{
					file_var_10 = btlAtelGetEntryPoint(regI0, 0);
					if (file_var_10 != -1)
					{
						file_var_11 = btlAtelGetPositionDataX(file_var_10);
						file_var_12 = btlAtelGetPositionDataZ(file_var_10);
						if (distance_290(-1, file_var_11, file_var_12) < 42)
						{
							if (btlAtelGetAngleDiff(-1, file_var_11, file_var_12) < 90)
							{
								file_var_8 = regI0;
								break;
							}
						}
					}
					else
					{
						file_var_8 = 0;
						break;
					}
				}
				if (file_var_8 >= 1)
				{
					if ((btlAtelGetDeadCountNow(file_var_8) < btlAtelGetDeadCountMax(file_var_8) || btlAtelGetDeadCountMax(file_var_8) == 0))
					{
						if ((btlAtelGetElapseTime(file_var_8) > btlAtelGetRespawnTime(file_var_8) || btlAtelGetDeadCountNow(file_var_8) == 0))
						{
							file_var_e = file_var_8;
							btlAtelSetKeepWorkGroup(file_var_8, -1);
							btlAtelIgnoreKeepWork(file_var_8, 0);
							setentrygroup(file_var_8);
							requestreentry();
							return;
						}
					}
				}
			}
			wait(1);
		}
	}


	function respawn01()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}


	function entry02()
	{
		btlAtelSetUnit(0);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 2);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(5, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn02()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if ((シナリオフラグ < 0x654 && battle_global_flag[2] == 0))
		{
			if (btlAtelDecideEntryGroupByContinuous() == -1)
			{
				file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
				if (file_var_8 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_8, -1);
					setentrygroup(file_var_8);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			if ((シナリオフラグ < 0x654 && battle_global_flag[2] == 0))
			{
				for (regI0 = 1; regI0 < 64; regI0 = (regI0 + 1))
				{
					file_var_10 = btlAtelGetEntryPoint(regI0, 0);
					if (file_var_10 != -1)
					{
						file_var_11 = btlAtelGetPositionDataX(file_var_10);
						file_var_12 = btlAtelGetPositionDataZ(file_var_10);
						if (distance_290(-1, file_var_11, file_var_12) < 42)
						{
							if (btlAtelGetAngleDiff(-1, file_var_11, file_var_12) < 90)
							{
								file_var_8 = regI0;
								break;
							}
						}
					}
					else
					{
						file_var_8 = 0;
						break;
					}
				}
				if (file_var_8 >= 1)
				{
					if ((btlAtelGetDeadCountNow(file_var_8) < btlAtelGetDeadCountMax(file_var_8) || btlAtelGetDeadCountMax(file_var_8) == 0))
					{
						if ((btlAtelGetElapseTime(file_var_8) > btlAtelGetRespawnTime(file_var_8) || btlAtelGetDeadCountNow(file_var_8) == 0))
						{
							file_var_e = file_var_8;
							btlAtelSetKeepWorkGroup(file_var_8, -1);
							btlAtelIgnoreKeepWork(file_var_8, 0);
							setentrygroup(file_var_8);
							requestreentry();
							return;
						}
					}
				}
			}
			wait(1);
		}
	}


	function respawn02()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}


	function entry03()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn03()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if ((シナリオフラグ < 0x654 && battle_global_flag[2] == 0))
		{
			if (btlAtelDecideEntryGroupByContinuous() == -1)
			{
				file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
				if (file_var_8 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_8, -1);
					setentrygroup(file_var_8);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			if ((シナリオフラグ < 0x654 && battle_global_flag[2] == 0))
			{
				for (regI0 = 1; regI0 < 64; regI0 = (regI0 + 1))
				{
					file_var_10 = btlAtelGetEntryPoint(regI0, 0);
					if (file_var_10 != -1)
					{
						file_var_11 = btlAtelGetPositionDataX(file_var_10);
						file_var_12 = btlAtelGetPositionDataZ(file_var_10);
						if (distance_290(-1, file_var_11, file_var_12) < 42)
						{
							if (btlAtelGetAngleDiff(-1, file_var_11, file_var_12) < 90)
							{
								file_var_8 = regI0;
								break;
							}
						}
					}
					else
					{
						file_var_8 = 0;
						break;
					}
				}
				if (file_var_8 >= 1)
				{
					if ((btlAtelGetDeadCountNow(file_var_8) < btlAtelGetDeadCountMax(file_var_8) || btlAtelGetDeadCountMax(file_var_8) == 0))
					{
						if ((btlAtelGetElapseTime(file_var_8) > btlAtelGetRespawnTime(file_var_8) || btlAtelGetDeadCountNow(file_var_8) == 0))
						{
							file_var_e = file_var_8;
							btlAtelSetKeepWorkGroup(file_var_8, -1);
							btlAtelIgnoreKeepWork(file_var_8, 0);
							setentrygroup(file_var_8);
							requestreentry();
							return;
						}
					}
				}
			}
			wait(1);
		}
	}


	function respawn03()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
char    file_var_9;              // pos: 0xd;


script lot_00(7)
{

	spawnData(9, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x3000179);
		btlAtelSetPoint2(0x30000ac);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(7);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry01()
	{
		btlAtelSetUnit(7);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn01()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn01()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry02()
	{
		btlAtelSetUnit(7);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn02()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn02()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry07()
	{
		btlAtelSetUnit(8);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn07()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn07()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry08()
	{
		btlAtelSetUnit(9);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn08()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn08()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}
}


script lot_01(7)
{

	spawnData(9, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x3000183);
		btlAtelSetPoint2(0x30000cc);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(7);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry01()
	{
		btlAtelSetUnit(7);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn01()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn01()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry02()
	{
		btlAtelSetUnit(7);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn02()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn02()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry07()
	{
		btlAtelSetUnit(8);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn07()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn07()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry08()
	{
		btlAtelSetUnit(9);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn08()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn08()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}
}


script lot_02(7)
{

	spawnData(9, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x300018d);
		btlAtelSetPoint2(0x30000ec);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(10);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(99, 0);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000020);
		btlAtelSetAbility(-0x36f00000, 0x4000040);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < redChocoHighChance)
						{
							file_var_9 = file_var_d;
						}
						else if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					else if (rand_29(100) < redChocoLowChance)
					{
						file_var_9 = file_var_d;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < redChocoHighChance)
						{
							file_var_9 = file_var_d;
						}
						else if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					else if (rand_29(100) < redChocoLowChance)
					{
						file_var_9 = file_var_d;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry01()
	{
		btlAtelSetUnit(7);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn01()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < redChocoHighChance)
						{
							file_var_9 = file_var_d;
						}
						else if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					else if (rand_29(100) < redChocoLowChance)
					{
						file_var_9 = file_var_d;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn01()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < redChocoHighChance)
						{
							file_var_9 = file_var_d;
						}
						else if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					else if (rand_29(100) < redChocoLowChance)
					{
						file_var_9 = file_var_d;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry02()
	{
		btlAtelSetUnit(7);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn02()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < redChocoHighChance)
						{
							file_var_9 = file_var_d;
						}
						else if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					else if (rand_29(100) < redChocoLowChance)
					{
						file_var_9 = file_var_d;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn02()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < redChocoHighChance)
						{
							file_var_9 = file_var_d;
						}
						else if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					else if (rand_29(100) < redChocoLowChance)
					{
						file_var_9 = file_var_d;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry07()
	{
		btlAtelSetUnit(8);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn07()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < redChocoHighChance)
						{
							file_var_9 = file_var_d;
						}
						else if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					else if (rand_29(100) < redChocoLowChance)
					{
						file_var_9 = file_var_d;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn07()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < redChocoHighChance)
						{
							file_var_9 = file_var_d;
						}
						else if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					else if (rand_29(100) < redChocoLowChance)
					{
						file_var_9 = file_var_d;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry08()
	{
		btlAtelSetUnit(9);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(19, 4);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(3, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000018);
		btlAtelSetAbility(0x1100000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn08()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < redChocoHighChance)
						{
							file_var_9 = file_var_d;
						}
						else if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					else if (rand_29(100) < redChocoLowChance)
					{
						file_var_9 = file_var_d;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn08()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			if (scratch2_var_13 < 6)
			{
				scratch2_var_13 = (scratch2_var_13 + 1);
			}
		}
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 >= 0)
				{
					btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
					setentrygroup(file_var_9);
					requestreentry();
					return;
				}
			}
		}
		while (true)
		{
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (((file_var_8 >= 1 && file_var_8 != file_var_b) && file_var_8 != file_var_c))
			{
				file_var_9 = btlAtelGetKeepWorkRealGroup(file_var_8);
				if (file_var_9 < 0)
				{
					if (scratch2_var_13 >= 6)
					{
						if (rand_29(100) < redChocoHighChance)
						{
							file_var_9 = file_var_d;
						}
						else if (rand_29(100) < 30)
						{
							file_var_9 = file_var_c;
						}
					}
					else if (scratch2_var_13 >= 3)
					{
						if (rand_29(100) < 50)
						{
							file_var_9 = file_var_b;
						}
					}
					else if (rand_29(100) < 1)
					{
						file_var_9 = file_var_c;
					}
					else if (rand_29(100) < 3)
					{
						file_var_9 = file_var_b;
					}
					else if (rand_29(100) < redChocoLowChance)
					{
						file_var_9 = file_var_d;
					}
					if (file_var_9 < 0)
					{
						file_var_9 = file_var_8;
					}
				}
				btlAtelSetKeepWorkGroup(file_var_9, file_var_8);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
			wait(1);
		}
	}
}


script monster_00(7)
{

	spawnData(7, true);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x3000197);
		btlAtelSetPoint2(0x300010c);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		return;
	}


	function spawn00()
	{
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry01()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn01()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn01()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry02()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn02()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn02()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry03()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn03()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn03()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry04()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn04()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn04()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry05()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn05()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn05()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry06()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn06()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn06()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}
}


script monster_01(7)
{

	spawnData(6, true);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x300019f);
		btlAtelSetPoint2(0x300012e);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		return;
	}


	function spawn00()
	{
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry01()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn01()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn01()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry02()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn02()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn02()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry03()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn03()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn03()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry04()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn04()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn04()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry05()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn05()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn05()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}
}


script monster_02(7)
{

	spawnData(6, true);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x30001a6);
		btlAtelSetPoint2(0x300014e);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		return;
	}


	function spawn00()
	{
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry01()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn01()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn01()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry02()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn02()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn02()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry03()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn03()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn03()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry04()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn04()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn04()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function entry05()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(20, 1);
		btlAtelSetEventFlagGroup(0, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(0);
		btlAtelSetSpecialSpawnRange(8);
		btlAtelSetDeSpawnType(1, 0, 0);
		btlAtelSetStatus(0x3000010);
		btlAtelSetAbility(0, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn05()
	{
		btlAtelSpawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}


	function respawn05()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_8 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_8 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if (btlAtelGetActiveActorNow() == btlAtelGetActiveActorMax())
			{
				btlAtelCanselInitialSpawnFlag();
			}
			file_var_8 = btlAtelDecideEntryPointAuto();
			if (file_var_8 >= 1)
			{
				btlAtelSetKeepWorkGroup(file_var_8, -1);
				setentrygroup(file_var_8);
				requestreentry();
				return;
			}
			wait(1);
		}
	}
}

//======================================================================
//                           Map exit arrays                            
//======================================================================

mapExitArray mapExitGroup0[11] = {

	exitStruct mapExit0 = {
		146.172882, -0, 238.327957, 1, 
		148.913803, -0.595661044, 240.885132, 0x10f
	};

	exitStruct mapExit1 = {
		148.913803, -0.595661044, 240.885132, 1, 
		151.425842, -0, 243.44455, 0x10f
	};

	exitStruct mapExit2 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit3 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit4 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit5 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit6 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit7 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit8 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit9 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit10 = {
		236.25589, -3.52969074, 340.762207, 1, 
		242.250107, -4.65474176, 345.749878, 0x30f
	};

};

mapExitArray mapExitGroup1[7] = {

	exitStruct mapExit0 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit1 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit2 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit3 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit4 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit5 = {
		36, -1, 327.394928, 1, 
		36, -2, 332, 0x20f
	};

	exitStruct mapExit6 = {
		36, -2, 332, 1, 
		36, -1, 336, 0x20f
	};

};

mapExitArray mapExitGroup2[0] = {

};

mapExitArray mapExitGroup3[0] = {

};

mapExitArray mapExitGroup4[0] = {

};


//======================================================================
//                      Map jump position vectors                       
//======================================================================
mjPosArr1 mapJumpPositions1[4] = {

	mjPos mapJumpPos0 = {
		144.829956, -0, 243.695267, 5.51159668,
		0, 0, 0, 0
	};

	mjPos mapJumpPos1 = {
		144.829956, -0, 243.695267, 5.51159668,
		0, 0, 0, 0
	};

	mjPos mapJumpPos2 = {
		40, -0, 332.117584, 1.66371024,
		0, 0, 0, 0
	};

	mjPos mapJumpPos3 = {
		235.17778, -0, 344.983704, 5.30790901,
		0, 0, 0, 0
	};

};
mjPosArr2 mapJumpPositions2[7] = {

	mjPos mapJumpPos0 = {
		0, 0, 0, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos1 = {
		204.942535, -0, 187.700638, 2.36266494,
		0, 0, 0, 0
	};

	mjPos mapJumpPos2 = {
		144.829956, -0, 243.695267, 5.51159668,
		0, 0, 0, 0
	};

	mjPos mapJumpPos3 = {
		-20.4244308, -0, 328.383392, 4.71238518,
		0, 0, 0, 0
	};

	mjPos mapJumpPos4 = {
		40, -0, 332.117584, 1.66371024,
		0, 0, 0, 0
	};

	mjPos mapJumpPos5 = {
		276.44751, -0, 306.767731, 2.45725846,
		0, 0, 0, 0
	};

	mjPos mapJumpPos6 = {
		235.17778, -0, 344.983704, 5.30790901,
		0, 0, 0, 0
	};

};




//======================================================================
//                          Unknown u16 Arrays                          
//======================================================================
unk16Arr2 unknown16Arrays2[4] = {
	unk16ArrEntry unknown16Array0 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array1 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array2 = {0, 0, 0, 0, 1, 48, 66, 0};
	unk16ArrEntry unknown16Array3 = {1, 115, 1, 1, 0, 65535, 0, 0};
};

